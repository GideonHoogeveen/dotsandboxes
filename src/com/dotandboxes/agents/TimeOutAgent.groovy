package com.dotandboxes.agents

import groovy.transform.CompileStatic;

import com.dotandboxes.board.Direction;
import com.dotandboxes.board.DotAndBoxesBoard;
import com.dotandboxes.board.LinePosition;
import com.dotandboxes.board.TilePosition;

@CompileStatic
class TimeOutAgent implements DotAndBoxesAgent {

    @Override
    public LinePosition getAction(DotAndBoxesBoard board) {
        Thread.sleep(300)
        return new LinePosition(new TilePosition(0,0), Direction.SOUTH)
    }

    @Override
    public String getName() {
        return "Time Out Agent"
    }

}
