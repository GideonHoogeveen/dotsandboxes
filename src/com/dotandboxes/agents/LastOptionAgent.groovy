package com.dotandboxes.agents

import com.dotandboxes.board.TilePosition;
import com.dotandboxes.board.DotAndBoxesBoard;
import com.dotandboxes.board.LinePosition;
import com.utils.LoopUtils;

import groovy.transform.CompileStatic;

@CompileStatic
class LastOptionAgent implements DotAndBoxesAgent {

    @Override
    public LinePosition getAction(DotAndBoxesBoard board) {
        
        for(int x = board.width-1; x >=0 ; x-- ) {
            for(int y = board.height-1; y >=0 ; y-- ) {
                Collection<LinePosition> emptyLinePositions = board.getEmptyLinePositions(new TilePosition(x, y))
                if(emptyLinePositions) {
                    return emptyLinePositions.last()
                }
            }
        }
        
        List<LinePosition> allEmptyLinePositions = []
        
        LoopUtils.doubleRangeLoop(board.width, board.height) { int x, int y ->
            allEmptyLinePositions.addAll(board.getEmptyLinePositions(new TilePosition(x, y)))
        }

        return allEmptyLinePositions.last()
    }
    
    @Override
    public String getName() {
        return 'Last Option Agent'
    }
    
}
