package com.dotandboxes.agents

import groovy.transform.CompileStatic

import com.dotandboxes.board.TilePosition
import com.dotandboxes.board.DotAndBoxesBoard
import com.dotandboxes.board.Line
import com.dotandboxes.board.LinePosition
import com.utils.LoopUtils

@CompileStatic
class RandomAgent implements DotAndBoxesAgent {

    @Override
    public LinePosition getAction(DotAndBoxesBoard board) {
        
        List<LinePosition> allEmptyLinePositions = []
        
        LoopUtils.doubleRangeLoop(board.width, board.height) { int x, int y ->
            allEmptyLinePositions.addAll(board.getEmptyLinePositions(new TilePosition(x, y)))
        }
        
        Random random = new Random()
        int index = random.nextInt(allEmptyLinePositions.size())
        
        board.drawLine(allEmptyLinePositions[index], Line.BLUE)
        
        return allEmptyLinePositions[index]
    }
    
    @Override
    public String getName() {
        return 'Random Agent'
    }
    
}
