package com.dotandboxes.agents

import com.dotandboxes.board.TilePosition;
import com.dotandboxes.board.DotAndBoxesBoard;
import com.dotandboxes.board.LinePosition;
import com.utils.LoopUtils;

import groovy.transform.CompileStatic;

@CompileStatic
class GeertsNewAgent implements DotAndBoxesAgent {

    @Override
    public LinePosition getAction(DotAndBoxesBoard board) {
        
        for(int x = 0; x < board.width; x++ ) {
            for(int y = 0; y < board.height; y++ ) {
                Collection<LinePosition> emptyLinePositions = board.getEmptyLinePositions(new TilePosition(x, y))
                if (emptyLinePositions.size()==1) {
                    return emptyLinePositions.first()
                }
             }
        }
		for(int x = 0; x < board.width; x++ ) {
			for(int y = 0; y < board.height; y++ ) {
				Collection<LinePosition> emptyLinePositions = board.getEmptyLinePositions(new TilePosition(x, y))

				if (emptyLinePositions.size()==3  ) {
					Collection<LinePosition> foundPositions =  emptyLinePositions.findAll { LinePosition lp -> emptyPositionsForMirror(lp, board)==3}
					if(foundPositions) {
						return foundPositions.first()
					}
				}
			 }
		}
		for(int x = 0; x < board.width; x++ ) {
			for(int y = 0; y < board.height; y++ ) {
				Collection<LinePosition> emptyLinePositions = board.getEmptyLinePositions(new TilePosition(x, y))

				if (emptyLinePositions.size()==3  ) {
					Collection<LinePosition> foundPositions =  emptyLinePositions.findAll { LinePosition lp -> emptyPositionsForMirror(lp, board)==4}
					if(foundPositions) {
						return foundPositions.first()
					}
				}
			 }
		}
		for(int x = 0; x < board.width; x++ ) {
			for(int y = 0; y < board.height; y++ ) {
				Collection<LinePosition> emptyLinePositions = board.getEmptyLinePositions(new TilePosition(x, y))

				if (emptyLinePositions.size()==4  ) {
					Collection<LinePosition> foundPositions =  emptyLinePositions.findAll { LinePosition lp -> emptyPositionsForMirror(lp, board)==3}
					if(foundPositions) {
						return foundPositions.first()
					}
				}
			 }
		}
		for(int x = 0; x < board.width; x++ ) {
			for(int y = 0; y < board.height; y++ ) {
				Collection<LinePosition> emptyLinePositions = board.getEmptyLinePositions(new TilePosition(x, y))

				if (emptyLinePositions.size()==4  ) {
					Collection<LinePosition> foundPositions =  emptyLinePositions.findAll { LinePosition lp -> emptyPositionsForMirror(lp, board)==4}
					if(foundPositions) {
						return foundPositions.first()
					}
				}
			 }
		}
		for(int x = 0; x < board.width; x++ ) {
			for(int y = 0; y < board.height; y++ ) {
				Collection<LinePosition> emptyLinePositions = board.getEmptyLinePositions(new TilePosition(x, y))
                if (emptyLinePositions.size()>2 ) {
                    return emptyLinePositions.first()
                }
			}
		}
        for(int x = 0; x < board.width; x++ ) {
            for(int y = 0; y < board.height; y++ ) {
                Collection<LinePosition> emptyLinePositions = board.getEmptyLinePositions(new TilePosition(x, y))
                if(emptyLinePositions) {
                    return emptyLinePositions.first()
                }
            }
        }
    }
	
	private int emptyPositionsForMirror(LinePosition linePos1, DotAndBoxesBoard board) {
		LinePosition mirrorPos = linePos1.mirror()
		TilePosition mirrorTile = mirrorPos.position
		if (!board.isValidPosition(mirrorTile)) {
			return 0
		}
		Collection<LinePosition> emptyLinePositions = board.getEmptyLinePositions(mirrorTile)
		return emptyLinePositions.size()
	}
	
	    
    @Override
    public String getName() {
        return 'De NewAgent van Geert'
    }
    
}
