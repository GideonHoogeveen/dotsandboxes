package com.dotandboxes.agents

import com.dotandboxes.board.TilePosition;
import com.dotandboxes.board.DotAndBoxesBoard;
import com.dotandboxes.board.LinePosition;
import com.utils.LoopUtils;

import groovy.transform.CompileStatic;

@CompileStatic
class GeertsAgent implements DotAndBoxesAgent {

    @Override
    public LinePosition getAction(DotAndBoxesBoard board) {
        
        for(int x = 0; x < board.width; x++ ) {
            for(int y = (board.height-1); y >= 0; y-- ) {
                Collection<LinePosition> emptyLinePositions = board.getEmptyLinePositions(new TilePosition(x, y))
                if (emptyLinePositions.size()==1) {
                    return emptyLinePositions.first()
                }
				else if (emptyLinePositions.size()>3) {
				   return emptyLinePositions.first()
				}
				else if (emptyLinePositions.size()>2) {
					return emptyLinePositions.first()
				 }
             }
        }
        for(int x = 0; x < board.width; x++ ) {
            for(int y = (board.height-1); y >= 0; y-- ) {
                Collection<LinePosition> emptyLinePositions = board.getEmptyLinePositions(new TilePosition(x, y))
                if(emptyLinePositions) {
                    return emptyLinePositions.first()
                }
            }
        }
    }
    
    @Override
    public String getName() {
        return 'De Agent van Geert'
    }
    
}
