package com.dotandboxes.agents

import groovy.transform.CompileStatic

import java.util.List;
import java.util.regex.Pattern.Pos;

import com.dotandboxes.board.Direction
import com.dotandboxes.board.DotAndBoxesBoard
import com.dotandboxes.board.LinePosition
import com.dotandboxes.board.TilePosition

@CompileStatic



class DoeMaarWatAgent implements DotAndBoxesAgent {

  
    @Override
    public LinePosition getAction(DotAndBoxesBoard board) {
        Collection<LinePosition> emptyLinePositions;
        
        HashMap<Integer,LinePosition> lines = new HashMap()
        for(int x = 0; x < board.width; x++ ) {
            for(int y = 0; y < board.height; y++ ) {
               emptyLinePositions = board.getEmptyLinePositions(new TilePosition(x, y));
               if (emptyLinePositions.size() < 1){
                   return emptyLinePositions.getAt(0);
               } else if (emptyLinePositions.size() == 2) {
                   continue;
               }
               LinePosition best;
               for (LinePosition line : emptyLinePositions){
                   List<TilePosition> other = board.getAdjacentTiles(line);
                   Collection<LinePosition> emptyLinePositionsOther = board.getEmptyLinePositions(new TilePosition(x, y));
                   if (emptyLinePositions.size() < 1){
                        return emptyLinePositions.getAt(0);
                   }
                   else if (emptyLinePositions.size() == 2) {
                        continue;
                   } else {
                       break;
                   }
                   
               }
               
               
               
            }
        }  
         return emptyLinePositions.first()
            
         
    }
    
    
    @Override
    public String getName() {
        return 'Doe maar Wat Agent'
    }
    
}
