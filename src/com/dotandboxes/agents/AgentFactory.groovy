package com.dotandboxes.agents

import groovy.transform.CompileStatic

@CompileStatic
enum AgentFactory {
    DOUWE_WICO_AGENT(DouweWicoAgent), ERIKS_AGENT(EriksAgent), FIRST_OPTION_AGENT(FirstOptionAgent), FOURTH_LINE_AGENT(FourthLineAgent),
    GEERTS_AGENT(GeertsAgent), GEERTS_NEW_AGENT(GeertsNewAgent), LAST_OPTION_AGENT(LastOptionAgent), NO_MISTAKE_AGENT(NoMistakeAgent),
    RANDOM_AGENT(RandomAgent), THUMBS_UP_AGENT(ThumbsUpAgent), PRETTY_SOLID_AGENT(PrettySolidAgent)
    
    
    public static final AgentFactory randomAgent = RANDOM_AGENT
    public static final AgentFactory firstOptionAgent = FIRST_OPTION_AGENT
    public static final AgentFactory lastOptionAgent = LAST_OPTION_AGENT
    public static final AgentFactory fourhtLineAgent = FOURTH_LINE_AGENT
    public static final AgentFactory noMistakeAgent = NO_MISTAKE_AGENT
    public static final AgentFactory prettySolidAgent = PRETTY_SOLID_AGENT
    
    
    public static final AgentFactory douweWicoAgent = DOUWE_WICO_AGENT
    public static final AgentFactory eriksAgent = ERIKS_AGENT
    public static final AgentFactory geertsAgent = GEERTS_AGENT
    public static final AgentFactory geertsNewAgent = GEERTS_NEW_AGENT
    public static final AgentFactory thumbsUpAgent = THUMBS_UP_AGENT
    
    public final Class<DotAndBoxesAgent> agent
    
    public AgentFactory(Class<DotAndBoxesAgent> agent) {
        this.agent = agent
    }
    
    public static List<DotAndBoxesAgent> getAllAgents() {
        return values().collect { AgentFactory agentFactory -> agentFactory.instantiate() }
    }
    
    public DotAndBoxesAgent instantiate() {
        DotAndBoxesAgent agent = this.agent.newInstance()
        return new SafetyAgentWrapper(agent)
    }
}
