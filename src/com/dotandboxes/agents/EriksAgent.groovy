package com.dotandboxes.agents

import com.dotandboxes.board.DotAndBoxesBoard
import com.dotandboxes.board.LinePosition
import com.dotandboxes.board.TilePosition
import groovy.transform.CompileStatic

@CompileStatic
class EriksAgent implements DotAndBoxesAgent {

    @Override
    public LinePosition getAction(DotAndBoxesBoard board) {
        
        for(int x = 0; x < board.width; x++ ) {
            for(int y = 0; y < board.height; y++ ) {
                Collection<LinePosition> emptyLinePositions = board.getEmptyLinePositions(new TilePosition(x, y))
                if(emptyLinePositions && emptyLinePositions.size() == 1) {
                    return emptyLinePositions.first()
                }
            }
        }

        for(int x = 0; x < board.width; x++ ) {
            for(int y = 0; y < board.height; y++ ) {
                Collection<LinePosition> emptyLinePositions = board.getEmptyLinePositions(new TilePosition(x, y))
                //println(emptyLinePositions)
                if(emptyLinePositions && emptyLinePositions.size() != 2 )  {
                    LinePosition linePosition = emptyLinePositions.first()
                    Collection<LinePosition> emptyLineMirrorPositions = board.getEmptyLinePositions(linePosition.mirror().position)
                    if (emptyLineMirrorPositions && emptyLineMirrorPositions.size() != 2) {
                        return emptyLinePositions.first()
                    }
                }
            }
        }

        for(int x = 0; x < board.width; x++ ) {
            for(int y = 0; y < board.height; y++ ) {
                Collection<LinePosition> emptyLinePositions = board.getEmptyLinePositions(new TilePosition(x, y))
                if(emptyLinePositions && emptyLinePositions.size() == 3) {
                    return emptyLinePositions.first()
                }
            }
        }
    }
    
    @Override
    public String getName() {
        return 'Erik\'s Agent'
    }
    
}
