package com.dotandboxes.agents

import com.dotandboxes.board.TilePosition;
import com.dotandboxes.board.DotAndBoxesBoard;
import com.dotandboxes.board.LinePosition;
import com.utils.LoopUtils;

import groovy.transform.CompileStatic;

@CompileStatic

class WicoAgent implements DotAndBoxesAgent {
	@Override
	public LinePosition getAction(DotAndBoxesBoard board) {

		if (board.history) {
			LinePosition lastLinePosition = board.history.last()
			LinePosition mirrorLinePosition = board.history.last().mirror()
            // levert de laatse positie van de tegenspeler een bijna gevuld box
			Collection<LinePosition> emptyHistoryPositions = board.getEmptyLinePositions(lastLinePosition.position)
			if (emptyHistoryPositions.size() == 1) {
				//println("History")
				//println(emptyHistoryPositions.first())
				return emptyHistoryPositions.first()
			}
			// levert de mirror van de laatse positie van de tegenspeler een bijna gevuld box
			if (board.isValidPosition(mirrorLinePosition.position)) {
				Collection<LinePosition> emptyMirrorHistoryPositions = board.getEmptyLinePositions(mirrorLinePosition.position)
				if (emptyMirrorHistoryPositions.size() == 1) {
					//println("Mirror History")
					//println(emptyMirrorHistoryPositions.first())
					return emptyMirrorHistoryPositions.first()
				}
			}
		}
		
		
        // Vind een box welke bijna niet gevuld is en zet de laatste zet.
		for(int x = 0; x < board.width; x++ ) {
			for(int y = 0; y < board.height; y++ ) {
				Collection<LinePosition> emptyLinePositions = board.getEmptyLinePositions(new TilePosition(x, y))
				if(emptyLinePositions.size() > 2) {
					//println("Veel lege plekken box")
					LinePosition linePos = emptyLinePositions.first()
					LinePosition mirrored = linePos.mirror()
					if(board.isValidPosition(mirrored.position)) {
						if(board.getEmptyLinePositions(mirrored.position).size() > 2) {
							return linePos
						}
					} 
				}
			}
		}
		
		// Vind een volgende box welke nog geen 2 kanten gevuld heeft==> geen 3e zijde vullen
		//   en waarvan de mirror dat ook niet gaat doen.
		for(int x = 0; x < board.width; x++ ) {
			for(int y = 0; y < board.height; y++ ) {
				Collection<LinePosition> emptyLinePositions = board.getEmptyLinePositions(new TilePosition(x, y))
				if(emptyLinePositions.size() > 1 && emptyLinePositions.size() < 3) {
					//println("No third line")
					LinePosition linePos = emptyLinePositions.first()
					LinePosition mirrored = linePos.mirror()
					if(board.isValidPosition(mirrored.position)) {
						if(board.getEmptyLinePositions(mirrored.position).size() > 1) {
							println	"Kies nieuwe positie welke 2 gevuld heeft"
							return linePos
						}
					}
				}
			}
		}

		
        // Wanneer er niets meer werkt dan deze maar kiezen
		for(int x = 0; x < board.width; x++ ) {
			for(int y = 0; y < board.height; y++ ) {
				Collection<LinePosition> emptyLinePositions = board.getEmptyLinePositions(new TilePosition(x, y))
				if(emptyLinePositions) {
					//println("Fallback")
					//println(emptyLinePositions.first())
					return emptyLinePositions.first()
				}
			}
		}
	}

	@Override
	public String getName() {
		return 'Wico Agent'
	}

}
