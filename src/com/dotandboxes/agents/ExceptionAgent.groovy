package com.dotandboxes.agents

import groovy.transform.CompileStatic;

import com.dotandboxes.board.DotAndBoxesBoard;
import com.dotandboxes.board.Line;
import com.dotandboxes.board.LinePosition;

@CompileStatic
class ExceptionAgent implements DotAndBoxesAgent {

    @Override
    public LinePosition getAction(DotAndBoxesBoard board) {
        LinePosition test
        test.position
        return null;
    }
    
    @Override
    public String getName() {
        return 'Exception Agent'
    }

}
