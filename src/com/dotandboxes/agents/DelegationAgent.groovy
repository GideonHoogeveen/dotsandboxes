package com.dotandboxes.agents

import groovy.transform.CompileStatic;

import com.dotandboxes.board.DotAndBoxesBoard;
import com.dotandboxes.board.LinePosition;

@CompileStatic
class DelegationAgent implements DotAndBoxesAgent {
    
    List<DotAndBoxesAgent> agents = []
    String name
    
    public DelegationAgent(List<DotAndBoxesAgent> agents, String name) {
        this.agents.addAll(agents)
        this.name = name
    }

    @Override
    public LinePosition getAction(DotAndBoxesBoard board) {
        if(agents) {
            DotAndBoxesAgent agent = agents.remove(0)
            agents.add(agent)
            return agent.getAction(board)
        }
        return null
    }

    @Override
    public String getName() {
        return name
    }

}
