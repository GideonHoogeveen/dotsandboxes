package com.dotandboxes.agents

import groovy.transform.CompileStatic

import com.dotandboxes.board.DotAndBoxesBoard
import com.dotandboxes.board.LinePosition
import com.dotandboxes.board.TilePosition

@CompileStatic
class NoMistakeAgent implements DotAndBoxesAgent {
    
    @Override
    public LinePosition getAction(DotAndBoxesBoard board) {
        
        List<LinePosition> linePositions = []
        
        for(int x = 0; x < board.width; x++ ) {
            for(int y = 0; y < board.height; y++ ) {
                linePositions.addAll(board.getEmptyLinePositions(new TilePosition(x, y)))
            }
        }
        
        return linePositions.max { evaluationFunction(it, board) }
        
    }
    
    private int evaluationFunction(LinePosition linePos, DotAndBoxesBoard board) {
        List<TilePosition> tiles = board.getAdjacentTiles(linePos)
        
        return (int) tiles.sum { TilePosition tilePos->
            return evaluationFunction(tilePos, board)
        }
    }
    
    private int evaluationFunction(TilePosition tilePos, DotAndBoxesBoard board) {
        int drawnLines = 4 - board.getEmptyLinePositions(tilePos).size()
        
        if( drawnLines == 3 ) {
            return 100
        }
        
        if (drawnLines == 2) {
            return -50
        }
                
        return 0
    }

    @Override
    public String getName() {
        return 'No Mistake agent'
    }

}
