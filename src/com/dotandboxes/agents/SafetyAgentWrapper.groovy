package com.dotandboxes.agents

import groovy.time.TimeCategory
import groovy.time.TimeDuration
import groovy.transform.CompileStatic

import com.dotandboxes.board.DotAndBoxesBoard
import com.dotandboxes.board.LinePosition
import com.dotandboxes.game.InvalidActionException

/**
 * Wraps a DotAndBoxesAgent and provides a way to constrain and validate the actions of this agent
 */
class SafetyAgentWrapper implements DotAndBoxesAgent {
    
    private DotAndBoxesAgent agent
    private int timeoutInMiliseconds
    
    public SafetyAgentWrapper(DotAndBoxesAgent agent) {
        this.agent = agent
        this.timeoutInMiliseconds = 200
    }
    
    public SafetyAgentWrapper(DotAndBoxesAgent agent, int timeoutInMiliseconds) {
        this.agent = agent
        this.timeoutInMiliseconds = timeoutInMiliseconds
    }

    @Override
    public LinePosition getAction(DotAndBoxesBoard board) {
        
        Date start = new Date()
        
        LinePosition linePos
        
        try
        {
            linePos =  agent.getAction(board)
        }
        catch (Exception e) {
            e.printStackTrace()
            println "agent: ${agent} threw an exception: ${e.message}"
            //throw new InvalidActionException(agent, "agent: ${agent} threw an exception: ${e.message}")
            linePos = null
        } catch (Error e) {
            e.printStackTrace()
            println "agent: ${agent} threw an exception: ${e.message}"
            //throw new InvalidActionException(agent, "agent: ${agent} threw an exception: ${e.message}")
            linePos = null
        }
        
        use(TimeCategory) {
            Date now = new Date()
            Date expired = start + timeoutInMiliseconds.milliseconds
            TimeDuration duration = now.minus(start)
            if(now > expired) {
                println "agent: ${agent} took longer then ${timeoutInMiliseconds} ms to return an action. total of ${duration.getSeconds() *1000 + duration.getMillis()} ms"
                linePos = null
                //throw new InvalidActionException(agent, "agent: ${agent} took longer then ${timeoutInMiliseconds} ms to return an action. total of ${duration.getSeconds() *1000 + duration.getMillis()} ms")
            }
        }
        
        return linePos
    }

    @Override
    public String getName() {
        agent.getName()
    }
    
    @Override
    public String toString() {
        agent.name
    }

}
