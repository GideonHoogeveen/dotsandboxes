package com.dotandboxes.agents

import com.dotandboxes.board.Direction
import com.dotandboxes.board.DotAndBoxesBoard
import com.dotandboxes.board.LinePosition
import com.dotandboxes.board.Tile
import com.dotandboxes.board.TilePosition

class BrutalAgent implements DotAndBoxesAgent {

	@Override
	public LinePosition getAction(DotAndBoxesBoard board) {
		
		Tile[][] tiles = board.board
		
		// find finishing moves :D
		LinePosition result = findLine(tiles) { Tile tile, int x, int y ->
			if (tile.emptyDirections.size() == 1) {
				return missingDirectionLinePosition(tile, x, y)
			} else {
				return null
			}
		}
		// find second line moves, check that we don't screw up
		if (!result) {
			result = findLine(tiles) { Tile tile, int x, int y ->
				if (tile.emptyDirections.size() == 3) {
					return checkedMissingDirectionLinePosition(board, tile, x, y)
				} else {
					return null
				}
			}
		}
		// find completely empty ones
		if (!result) {
			result = findLine(tiles) { Tile tile, int x, int y ->
				if (tile.emptyDirections.size() == 4) {
					return missingDirectionLinePosition(tile, x, y)
//					return checkedMissingDirectionLinePosition(board, tile, x, y)
				} else {
					return null
				}
			}
		}
		if (!result) {
			while (!result || !board.isEmpty(result)) {
				result = random(board)
			}
		}
		
		return result
	}
	
	LinePosition checkedMissingDirectionLinePosition(DotAndBoxesBoard board, Tile tile, int x, int y) {
		LinePosition line = new LinePosition(new TilePosition(x, y), tile.emptyDirections[0])
		List<TilePosition> adjacentTiles = board.getAdjacentTiles(line)
		for (int c = 0; c < tile.emptyDirections.size(); c++) {
			line = new LinePosition(new TilePosition(x, y), tile.emptyDirections[c])
			
			boolean badMove = adjacentTiles.any { TilePosition tp ->
				Tile needle = board.board[tp.x][tp.y]
				return needle.getEmptyDirections().size() == 1
			}
			if (!badMove) {
				return line
			}
		}
		return null
	}
	
	LinePosition random(DotAndBoxesBoard board) {
		int y = Math.random() * board.height
		int x = Math.random() * board.width
		
		new LinePosition(new TilePosition(x, y), randomDirection())
	}
	
	Direction randomDirection() {
		int dirChoice = Math.random() * 4
		return Direction.values()[dirChoice]
	}
	
	LinePosition findLine(Tile[][] tiles, Closure finder) {
		for (int x = 0; x < tiles.length; x++) {
			for (int y = 0; y < tiles[x].length; y++)  {
				Tile tile = tiles[x][y]
				if (!tile.full) {
					LinePosition result = finder.call(tile, x, y)
					if (result) {
						return result
					}
				}
			}
		}
		return null
	}
	
	LinePosition missingDirectionLinePosition(Tile tile, int x, int y) {
		new LinePosition(new TilePosition(x, y), tile.emptyDirections[0]) 
	}

	@Override
	public String getName() {
		return "Brutal Agent";
	}

}
