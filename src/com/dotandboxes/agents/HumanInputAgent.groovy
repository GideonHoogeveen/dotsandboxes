package com.dotandboxes.agents

import groovy.transform.CompileStatic;

import com.dotandboxes.board.TilePosition
import com.dotandboxes.board.Direction
import com.dotandboxes.board.DotAndBoxesBoard
import com.dotandboxes.board.LinePosition

@CompileStatic
class HumanInputAgent implements DotAndBoxesAgent {

    
    private static final Map<String, Direction> directionMapping = [north: Direction.NORTH, east: Direction.EAST, south : Direction.SOUTH, west: Direction.WEST]
        
    
    @Override
    public LinePosition getAction(DotAndBoxesBoard board) {
        
        while(true) {
            String[] input = askInput()
            if(validateInput(input)) {
                LinePosition linePos = createLinePositionGivenInput(input)
                if(validateLinePosition(board, linePos)) {
                    return linePos
                }
            }
        }
        
    }
    
    private String[] askInput() {
        println "Enter x, y position and direction. i.e. 0 0 south : "
        return System.in.newReader().readLine().split(' ')
    }
    
    private boolean validateInput(String[] input) {        
        if(input?.length != 3) {
            println 'enter exactly 3 parameters'
            return false
        }
        
        if(!input[0].isInteger() || !input[1].isInteger()) {
            println 'first two parameters should be integers'
            return false
        }
        
        int x = input[0].toInteger()
        int y = input[1].toInteger()
        
        if(!(input[2].toLowerCase() in ['north', 'east', 'south', 'west'])) {
            println "last parameter should be in the list ['north', 'east', 'south', 'west']"
            return false
        }
        
        return true
        
    }
    
    private LinePosition createLinePositionGivenInput(String[] input) {
        int x = input[0].toInteger()
        int y = input[1].toInteger()
        String dir = input[2]
        
        Direction direction = directionMapping[dir.toLowerCase()]
        
        return new LinePosition(new TilePosition(x, y), direction)
    }
    
    private boolean validateLinePosition(DotAndBoxesBoard board, LinePosition linePos) {
        
        if(!board.isValidPosition(linePos.position)) {
            println 'position should be a valid position on the gameboard'
            return false
        }
        
        if(!board.getLine(linePos).isEmpty()) {
            println "line indicated by the given input has already been drawn on"
            return false
        }
        
        return true
        
    }
    
    @Override
    public String getName() {
        return 'Human Input Agent'
    }

}
