package com.dotandboxes.agents;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.dotandboxes.board.Direction;
import com.dotandboxes.board.DotAndBoxesBoard;
import com.dotandboxes.board.Line;
import com.dotandboxes.board.LinePosition;
import com.dotandboxes.board.TilePosition;

public class OneBigMistakeAgent implements DotAndBoxesAgent {

	@Override
	public LinePosition getAction(DotAndBoxesBoard board) {
		Map<LinePosition, Integer> whatever = new HashMap<>();
		
        for(int x = 0; x < board.width; x++ ) {
            for(int y = 0; y < board.height; y++ ) {
                Collection<LinePosition> emptyLinePositions = board.getEmptyLinePositions(new TilePosition(x, y));
                if (emptyLinePositions.size() == 1) {
                	return emptyLinePositions.iterator().next();
                }
				for(LinePosition lp : emptyLinePositions) {
					
					whatever.put(lp, score(emptyLinePositions.size()));
                }
            }
        }
        
        for (LinePosition lp : whatever.keySet()) {
        	Integer score = whatever.get(lp);
        	LinePosition mirror = lp.mirror();
        	if (whatever.containsKey(mirror)) {
        		Integer mirrorScore = whatever.get(mirror);
        		Integer minScore = Math.min(mirrorScore, score);
        		whatever.put(lp, minScore);
        		whatever.put(mirror, minScore);
        	}
        }
        
        Integer bestScore = Integer.MIN_VALUE;
        LinePosition bestPosition = null;
        for (LinePosition lp : whatever.keySet()) {
        	if (whatever.get(lp) > bestScore) {
        		bestScore = whatever.get(lp);
        		bestPosition = lp;
        	}
        }
        Integer smallestGiveaway = Integer.MAX_VALUE;
        if (bestScore == -1) {
        	//waitF(1000);
        	//shit, we need to give our opponent something, let's give something small...
        	for (LinePosition lp : whatever.keySet()) {
        		if (whatever.get(lp) == -1) {
        			Integer giveaway = getGiveaway(board, lp);
        			if (giveaway < smallestGiveaway) {
        				smallestGiveaway = giveaway;
        				bestPosition = lp;
        			}
        		}
        	}
        }
        return bestPosition;
        
//        List<Integer> scores = new ArrayList<Integer>(whatever.keySet());
//        Collections.sort(scores);
//        Integer bestScore = scores.get(scores.size()-1);
//        System.out.println("best score is " + bestScore);
//        List<LinePosition> bestScoringPositions = whatever.get(bestScore);
        
	}

	private void waitF(int h) {
		try {
			Thread.sleep(h);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private Integer getGiveaway(DotAndBoxesBoard board, LinePosition lp) {
		board = board.clone();
		board.drawLine(lp, Line.RED);
		
		int up = getGiveaway(board, lp.getPosition());
		int down = getGiveaway(board, lp.mirror().getPosition());
		return Math.max(up, down);
	}

	private int getGiveaway(DotAndBoxesBoard board, TilePosition position) {
		if (!board.isValidPosition(position)) {
			return 0;
		}
	
		Set<TilePosition> reachable = new HashSet<TilePosition>();
		reachable.add(position);
		boolean doneSomething = true;
		while(doneSomething) {
			doneSomething = false;
			Set<TilePosition> newReachable = new HashSet<TilePosition>();
			for (TilePosition tp : reachable) {
				Set<TilePosition> neighbors = getReachableNeighbors(board, tp);
				for (TilePosition tp2 : neighbors) {
					if (!reachable.contains(tp2)) {
						newReachable.add(tp2);
						doneSomething = true;
					}
				}
			}
			reachable.addAll(newReachable);
		}
		
		return reachable.size();
		
	}

	private Set<TilePosition> getReachableNeighbors(DotAndBoxesBoard board, TilePosition tp) {
		Set<TilePosition> reachableNeighbors = new HashSet<TilePosition>();
		for (Direction d : Direction.values()) {
			TilePosition tp2 = tp.plus(d);
			if (board.isValidPosition(tp2)) {
				if (!board.isDrawn(new LinePosition(tp, d))) {
					reachableNeighbors.add(tp2);					
				}
			}
		}
		return reachableNeighbors;
	}

	private Integer score(int nrOfEmptyLinePositions) {
		switch (nrOfEmptyLinePositions) {
		case 1:
			return 10;
		case 2:
			return -1;
		case 3:
			return 1;
		case 4:
			return 0;
		}
		throw new IllegalStateException("Hoe kan dit nou?");
	}
	
	@Override
	public String getName() {
		return "Brams coole agent";
	}

}
