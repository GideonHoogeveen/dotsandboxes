package com.dotandboxes.agents

import groovy.transform.CompileStatic

import com.dotandboxes.board.DotAndBoxesBoard
import com.dotandboxes.board.LinePosition
import com.dotandboxes.board.TilePosition
import com.dotandboxes.graph.Graph
import com.dotandboxes.graph.Node

@CompileStatic
class PrettySolidAgent implements DotAndBoxesAgent {
    
    Random random = new Random()

    @Override
    public LinePosition getAction(DotAndBoxesBoard board) {
        
        List<LinePosition> linePositions = []
        
        for(int x = 0; x < board.width; x++ ) {
            for(int y = 0; y < board.height; y++ ) {
                linePositions.addAll(board.getEmptyLinePositions(new TilePosition(x, y)))
            }
        }
        
        LinePosition linePos = linePositions.max {LinePosition linePos -> evaluationFunction(linePos, board) }
        
        if(evaluationFunction(linePos, board) >= 0) {
            return linePos
        }
        
        
        Set<TilePosition> tilePositions = linePositions.collect { it.position} as Set
        
        
        
        Graph<TilePosition> graph = addNodesToGraph(tilePositions, board)
        
        Node<TilePosition> node =  graph.nodes.findAll {it.getPathLength() > 0}.min {it.getPathLength()}
        
        return board.getEmptyLinePositions(node.value).first()
    }
    

    private Graph<TilePosition> addNodesToGraph(Set<TilePosition> tilePositions, DotAndBoxesBoard board) {
        
        Graph<TilePosition> graph = new Graph<TilePosition>()
        
        for(TilePosition tilePos : tilePositions) {
            graph.addNode(new Node<TilePosition>(tilePos))
        }
        
        for(TilePosition tilePos : tilePositions) {
            Collection<LinePosition> linePositions = board.getEmptyLinePositions(tilePos)
            
            for(LinePosition linePos: linePositions) {
                List<TilePosition> tiles = board.getAdjacentTiles(linePos)
                if(tiles.size() == 2) {
                    Node startNode = graph.getNode(tiles[0])
                    Node endNode = graph.getNode(tiles[1])
                    startNode.add(endNode)
                    endNode.add(startNode)
                }
            }
            
        }
        
        
        return graph        
    }
    
    private int evaluationFunction(LinePosition linePos, DotAndBoxesBoard board) {
        List<TilePosition> tiles = board.getAdjacentTiles(linePos)
        
        return (int) tiles.sum { TilePosition tilePos->
            return evaluationFunction(tilePos, board)
        }
    }
    
    private int evaluationFunction(TilePosition tilePos, DotAndBoxesBoard board) {
        int drawnLines = board.getEmptyLinePositions(tilePos).size()
        
        if( drawnLines == 1 ) {
            return 100
        }
        
        if (drawnLines == 2) {
            return -50
        }
                
        return random.nextInt(5) + 1
    }

    @Override
    public String getName() {
        return 'Pretty Solid Agent'
    }

}
