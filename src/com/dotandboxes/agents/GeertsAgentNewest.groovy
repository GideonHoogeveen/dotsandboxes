package com.dotandboxes.agents

import groovy.transform.CompileStatic

import com.dotandboxes.board.DotAndBoxesBoard
import com.dotandboxes.board.LinePosition
import com.dotandboxes.board.TilePosition

@CompileStatic
class GeertsAgentNewest implements DotAndBoxesAgent {

    @Override
    public LinePosition getAction(DotAndBoxesBoard board) {
		
// First search for possible last move
		LinePosition lp = selectedMove(board, 1)
		if (lp) return lp

// Search for tiles with 3 or 4 free positions, mirrored tiles 3, 4 free positions (or no mirror)		        
		for(int x = 3; x < 5; x++ ) {
			for(int y = 3; y < 6; y++ ) {
				lp = selectedMove(board, x, y)
				if (lp) return lp
			}
		}	

//Search for tile with 3 free positions , regardless free position of mirrored tiles				
		lp = selectedMove(board, 3)
		if (lp) return lp

//Search for the only remaining option, tiles with 2 free positions		
		lp = selectedMove(board, 2)
		if (lp) return lp
    }
	
	LinePosition selectedMove(DotAndBoxesBoard board, int emptyPos, int emptyPosMirror) {
		for(int x = 0; x < board.width; x++ ) {
			for(int y = 0; y < board.height; y++ ) {
				Collection<LinePosition> emptyLinePositions = board.getEmptyLinePositions(new TilePosition(x, y))

				if (emptyLinePositions.size()==emptyPos  ) {
					Collection<LinePosition> foundPositions =  emptyLinePositions.findAll { LinePosition lp -> emptyPositionsForMirror(lp, board)==emptyPosMirror}
					if(foundPositions) {
						return foundPositions.first()
					}
				}
			 }
		}
	}

	LinePosition selectedMove(DotAndBoxesBoard board, int emptyPos) {
		for(int x = 0; x < board.width; x++ ) {
			for(int y = 0; y < board.height; y++ ) {
				Collection<LinePosition> emptyLinePositions = board.getEmptyLinePositions(new TilePosition(x, y))
                if (emptyLinePositions.size()==emptyPos ) {
					return emptyLinePositions.first()
                }
			}
		}
	}

	 
	private int emptyPositionsForMirror(LinePosition linePos1, DotAndBoxesBoard board) {
		LinePosition mirrorPos = linePos1.mirror()
		TilePosition mirrorTile = mirrorPos.position
		if (!board.isValidPosition(mirrorTile)) {
			return 5
		}
		Collection<LinePosition> emptyLinePositions = board.getEmptyLinePositions(mirrorTile)
		return emptyLinePositions.size()
	}
	
   @Override
    public String getName() {
        return 'Geerts Agent2'
    }
}
