package com.dotandboxes.agents

import com.dotandboxes.board.TilePosition;
import com.dotandboxes.board.DotAndBoxesBoard;
import com.dotandboxes.board.LinePosition;
import com.utils.LoopUtils;

import groovy.transform.CompileStatic;

@CompileStatic
class ExampleAgent implements DotAndBoxesAgent {

    @Override
    public LinePosition getAction(DotAndBoxesBoard board) {
        
        for(int x = 0; x < board.width; x++ ) {
            for(int y = 0; y < board.height; y++ ) {
                Collection<LinePosition> emptyLinePositions = board.getEmptyLinePositions(new TilePosition(x, y))
                if(emptyLinePositions) {
                    return emptyLinePositions.first()
                }
            }
        }
    }
    
    @Override
    public String getName() {
        return 'Example Agent'
    }
    
}
