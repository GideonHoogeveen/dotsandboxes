package com.dotandboxes.agents

import com.dotandboxes.board.TilePosition;
import com.dotandboxes.board.DotAndBoxesBoard;
import com.dotandboxes.board.LinePosition;
import com.utils.LoopUtils;

import groovy.transform.CompileStatic;

@CompileStatic
class DouweWicoAgent implements DotAndBoxesAgent {

    @Override
    public LinePosition getAction(DotAndBoxesBoard board) {

        if (board.history) {
            LinePosition lastLinePosition = board.history.last()
            LinePosition mirrorLinePosition = board.history.last().mirror()

            Collection<LinePosition> emptyHistoryPositions = board.getEmptyLinePositions(lastLinePosition.position)
            if (emptyHistoryPositions.size() == 1) {
                //println("History")
                //println(emptyHistoryPositions.first())
                return emptyHistoryPositions.first()
            }

            if (board.isValidPosition(mirrorLinePosition.position)) {
                Collection<LinePosition> emptyMirrorHistoryPositions = board.getEmptyLinePositions(mirrorLinePosition.position)
                if (emptyMirrorHistoryPositions.size() == 1) {
                    //println("Mirror History")
                    //println(emptyMirrorHistoryPositions.first())
                    return emptyMirrorHistoryPositions.first()
                }
            }
        }


        for(int x = 0; x < board.width; x++ ) {
            for(int y = 0; y < board.height; y++ ) {
                Collection<LinePosition> emptyLinePositions = board.getEmptyLinePositions(new TilePosition(x, y))
                if(emptyLinePositions.size() > 2) {
                    //println("No third line")
                    LinePosition linePos = emptyLinePositions.first()
                    LinePosition mirrored = linePos.mirror()
                    if(board.isValidPosition(mirrored.position)) {
                        if(board.getEmptyLinePositions(mirrored.position).size() > 2) {
                            return linePos
                        }
                    } else {
                        return emptyLinePositions.first()
                    }
                }
            }
        }

        for(int x = 0; x < board.width; x++ ) {
            for(int y = 0; y < board.height; y++ ) {
                Collection<LinePosition> emptyLinePositions = board.getEmptyLinePositions(new TilePosition(x, y))
                if(emptyLinePositions) {
                    //println("Fallback")
                    //println(emptyLinePositions.first())
                    return emptyLinePositions.first()
                }
            }
        }
    }

    @Override
    public String getName() {
        return 'Douwe Wico Agent'
    }

}
