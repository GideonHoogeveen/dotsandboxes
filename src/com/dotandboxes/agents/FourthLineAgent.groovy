package com.dotandboxes.agents

import groovy.transform.CompileStatic

import com.dotandboxes.board.DotAndBoxesBoard
import com.dotandboxes.board.LinePosition
import com.dotandboxes.board.TilePosition

@CompileStatic
class FourthLineAgent implements DotAndBoxesAgent {
    
    DotAndBoxesAgent fallbackAgent
    
    public FourthLineAgent() {
        fallbackAgent = new RandomAgent()
    }

    @Override
    public LinePosition getAction(DotAndBoxesBoard board) {
        
        if(!board.history) {
            return fallbackAgent.getAction(board)
        }
        
        TilePosition firstTile = board.history.last().position
        TilePosition secondTile = board.history.last().mirror().position
       
       Collection<LinePosition> emptyLinePositions = board.getEmptyLinePositions(firstTile)
       
       if(emptyLinePositions.size() == 1) {
           return emptyLinePositions.first()
       } else if (board.isValidPosition(secondTile)) {
           emptyLinePositions = board.getEmptyLinePositions(secondTile)
           if(emptyLinePositions.size() == 1) {
               return emptyLinePositions.first()
           }
       }
        
       return fallbackAgent.getAction(board)
    }

    @Override
    public String getName() {
        return 'Fourth Line agent'
    }

}
