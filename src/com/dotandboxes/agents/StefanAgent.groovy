package com.dotandboxes.agents

import com.dotandboxes.board.DotAndBoxesBoard
import com.dotandboxes.board.LinePosition
import com.dotandboxes.board.TilePosition
import groovy.transform.CompileStatic

@CompileStatic
class StefanAgent implements DotAndBoxesAgent {

    @Override
    public LinePosition getAction(DotAndBoxesBoard board) {

        for (int x = 0; x < board.width; x++) {
            for (int y = 0; y < board.height; y++) {
                Collection<LinePosition> emptyLinePositions = board.getEmptyLinePositions(new TilePosition(x, y))
                if (emptyLinePositions) {
                    // Sluiten hokjes bij 1 empty line:
                    if (emptyLinePositions.size() == 1)
                    // Ik zeg doen! :
                        return emptyLinePositions.first()

                }
            }
        }

        for (int x = 0; x < board.width; x++) {
            for (int y = 0; y < board.height; y++) {
                Collection<LinePosition> emptyLinePositions = board.getEmptyLinePositions(new TilePosition(x, y))
                for (LinePosition linePos : emptyLinePositions) {
                    // Kijken of line pos is 3e lijntje in zelf en aanligende vakjes
                    println('Kijken aanliggende tiles '+linePos+':')
                    for (TilePosition tile : board.getAdjacentTiles(linePos)) {
                        println(' Tile:'+tile)
                        Collection<LinePosition> emptyLinePositions2 = board.getEmptyLinePositions(tile)

                        if (emptyLinePositions2.size() == 2) break
                        //NORTH:
                        //EAST 2?

                        //SOUTH 2?
                        //WEST 2?

                    }

                }

            }
        }


    // Als niets lukt, dan maar de eerste:
    return fallBack ( board )
}

public LinePosition fallBack(DotAndBoxesBoard board) {
println('Fallback!')
    for (int x = 0; x < board.width; x++) {
        for (int y = 0; y < board.height; y++) {
            Collection<LinePosition> emptyLinePositions = board.getEmptyLinePositions(new TilePosition(x, y))
            if (emptyLinePositions) {
                return emptyLinePositions.first()
            }
        }
    }
}

@Override
public String getName() {
    return 'Stefan\'s Option Agent'
}

}
