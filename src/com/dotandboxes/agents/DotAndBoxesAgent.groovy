package com.dotandboxes.agents

import groovy.transform.CompileStatic;

import com.dotandboxes.board.DotAndBoxesBoard;
import com.dotandboxes.board.LinePosition;

@CompileStatic
interface DotAndBoxesAgent {
    
    public LinePosition getAction(DotAndBoxesBoard board)
    
    public String getName()
    
}
