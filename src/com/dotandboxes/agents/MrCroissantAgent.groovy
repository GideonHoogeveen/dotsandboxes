package com.dotandboxes.agents

import com.dotandboxes.board.Direction
import static com.dotandboxes.board.Direction.*
import com.dotandboxes.board.DotAndBoxesBoard
import com.dotandboxes.board.LinePosition
import com.dotandboxes.board.TilePosition

class MrCroissantAgent implements DotAndBoxesAgent {

	int planCounter = 0
	
	LinePosition lastLine
	
	@Override
	public LinePosition getAction(DotAndBoxesBoard board) {
		LinePosition line = createAction(board)
		
		while (!valid(board, line)) {
			line = createAction(board)
		}
		
		lastLine = line
		return line
	}
	
	LinePosition createAction(DotAndBoxesBoard board) {
		LinePosition result
		
		switch (planCounter) {
			case 0: result = createFirst(board)
				break
			default: result = createNext(board)
		}
		
		planCounter++
		if (planCounter > 3) {
			planCounter = 0
		}
		
		return result
	}
	
	LinePosition createNext(DotAndBoxesBoard board) {
		new LinePosition(lastLine.position, lastLine.direction.clockWise())
	}
	
	LinePosition createFirst(DotAndBoxesBoard board) {
		int y = Math.random() * board.height
		int x = Math.random() * board.width
		
		new LinePosition(new TilePosition(x, y), randomDirection())
	}
	
	Direction randomDirection() {
		int dirChoice = Math.random() * 4
		return Direction.values()[dirChoice]
	}
	
	boolean valid(DotAndBoxesBoard board, LinePosition line) {
		boolean valid = board.isEmpty(line)
		
		if (!valid) {
			planCounter = 0
		}
		
		return valid
	}

	@Override
	public String getName() {
		return "Agent de Mr. Croissant"
	}
	
}
