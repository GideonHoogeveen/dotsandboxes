package com.dotandboxes.agents

import groovy.transform.CompileStatic

import com.dotandboxes.board.Direction
import com.dotandboxes.board.DotAndBoxesBoard
import com.dotandboxes.board.LinePosition
import com.dotandboxes.board.TilePosition

//@CompileStatic 
class ThumbsUpAgent implements DotAndBoxesAgent {

    @Override
    public LinePosition getAction(DotAndBoxesBoard board) {
        
        
        //[x,y,z]
        List pixelArt = [
        [0,4,Direction.WEST],
        [0,4,Direction.NORTH],
        [0,4,Direction.EAST],
        [0,4,Direction.SOUTH],
        [1,4,Direction.NORTH],
        [1,4,Direction.EAST],
        [1,4,Direction.SOUTH],
        [3,4,Direction.WEST],
        [3,4,Direction.NORTH],
        [3,4,Direction.EAST],
        [3,4,Direction.SOUTH],
        
        [4,2,Direction.WEST],
        [4,2,Direction.NORTH],
        [4,2,Direction.EAST],
        [4,3,Direction.EAST],
        [4,3,Direction.SOUTH],
        [4,3,Direction.WEST],
        [4,3,Direction.NORTH],
        
        [0,5,Direction.WEST],
        [0,5,Direction.NORTH],
        [0,5,Direction.EAST],
        [0,5,Direction.SOUTH],
        [1,5,Direction.NORTH],
        [1,5,Direction.EAST],
        [1,5,Direction.SOUTH],
        [2,5,Direction.NORTH],
        [2,5,Direction.EAST],
        [2,5,Direction.SOUTH],

        [0,6,Direction.WEST],
        [0,6,Direction.NORTH],
        [0,6,Direction.EAST],
        [0,6,Direction.SOUTH],
        [1,6,Direction.NORTH],
        [1,6,Direction.EAST],
        [1,6,Direction.SOUTH],

        [0,7,Direction.WEST],
        [0,7,Direction.NORTH],
        [0,7,Direction.EAST],
        [0,7,Direction.SOUTH],
        [1,7,Direction.NORTH],
        [1,7,Direction.EAST],
        [1,7,Direction.SOUTH],
        [2,7,Direction.NORTH],
        [2,7,Direction.EAST],
        [2,7,Direction.SOUTH],
        [3,7,Direction.NORTH],
        [3,7,Direction.EAST],
        [3,7,Direction.SOUTH],
              
        [0,8,Direction.WEST],
        [0,8,Direction.NORTH],
        [0,8,Direction.EAST],
        [0,8,Direction.SOUTH],
        [1,8,Direction.NORTH],
        [1,8,Direction.EAST],
        [1,8,Direction.SOUTH],
        
        [4,8,Direction.WEST],
        [4,8,Direction.NORTH],
        [4,8,Direction.EAST],
        [4,8,Direction.SOUTH],
        [5,8,Direction.NORTH],
        [5,8,Direction.EAST],
        [5,8,Direction.SOUTH],
        [6,8,Direction.NORTH],
        [6,8,Direction.EAST],
        [6,8,Direction.SOUTH],
        [6,8,Direction.NORTH],
        [6,8,Direction.EAST],
        [6,8,Direction.SOUTH],
        [7,8,Direction.NORTH],
        [7,8,Direction.EAST],
        [7,8,Direction.SOUTH],
        
        [5,1,Direction.WEST],
        [5,1,Direction.NORTH],
        [5,1,Direction.EAST],
        [5,1,Direction.SOUTH],
        [6,1,Direction.NORTH],
        [6,1,Direction.EAST],
        [6,1,Direction.SOUTH],
        [6,2,Direction.EAST],
        [6,2,Direction.SOUTH],
        [6,2,Direction.WEST],
        [6,3,Direction.EAST],
        [6,3,Direction.SOUTH],
        [6,3,Direction.WEST],
        [6,4,Direction.EAST],
        [6,4,Direction.SOUTH],
        [6,4,Direction.WEST],

        [7,4,Direction.NORTH],
        [7,4,Direction.EAST],
        [7,4,Direction.SOUTH],

        [8,5,Direction.NORTH],
        [8,5,Direction.EAST],
        [8,5,Direction.SOUTH],
        [8,5,Direction.WEST],
        [8,6,Direction.EAST],
        [8,6,Direction.SOUTH],
        [8,6,Direction.WEST],
        [8,7,Direction.EAST],
        [8,7,Direction.SOUTH],
        [8,7,Direction.WEST],

        // Opvullen van de Like
        
        [5,2,Direction.SOUTH],
        [5,3,Direction.SOUTH],
        [5,4,Direction.SOUTH],
        [5,4,Direction.WEST],
        [4,4,Direction.SOUTH],
        [2,6,Direction.EAST],
        [3,5,Direction.EAST],
        [3,5,Direction.SOUTH],
        [3,6,Direction.EAST],
        [3,6,Direction.SOUTH],
        [4,5,Direction.EAST],
        [4,5,Direction.SOUTH],
        [4,6,Direction.EAST],
        [4,6,Direction.SOUTH],
        [4,7,Direction.EAST],
        
        [5,5,Direction.EAST],
        [5,5,Direction.SOUTH],
        [5,6,Direction.EAST],
        [5,6,Direction.SOUTH],
        [5,7,Direction.EAST],

        [6,5,Direction.EAST],
        [6,5,Direction.SOUTH],
        [6,6,Direction.EAST],
        [6,6,Direction.SOUTH],
        [6,7,Direction.EAST],

        [7,5,Direction.EAST],
        [7,5,Direction.SOUTH],
        [7,6,Direction.EAST],
        [7,6,Direction.SOUTH],
        [7,7,Direction.EAST]

        ]
        
        for (List thumb: pixelArt) {
            
            LinePosition pixelLine = new LinePosition(new TilePosition (thumb[0],thumb[1]), thumb[2])
            
            if (board.getLine(pixelLine).isEmpty()) {
                return pixelLine
            }
            
        }
        //haal eerste uit lijst
        //check of al getekend
        //herhaal stap
        //anders geef zet terug
        
        
        return new LinePosition(new TilePosition (0,4), Direction.NORTH);
    }

    @Override
    public String getName() {
        return "ThumbsUp";
    }
    
}
