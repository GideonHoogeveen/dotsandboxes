package com.dotandboxes.board

import groovy.transform.CompileStatic
import groovy.transform.Immutable

@CompileStatic
@Immutable
class BoardStatistics {
    
    int emptyTiles
    int blueTiles
    int redTiles
    
    public int getTotalTiles() {
        return emptyTiles + blueTiles + redTiles
    }
    
    public boolean isWon() {
        return blueTiles * 2 > totalTiles || redTiles * 2 > totalTiles
    }
    
    public Line getWinningColor() {        
        if(redTiles != blueTiles) {
            return redTiles > blueTiles ? Line.RED : Line.BLUE
        }
        return Line.NONE
    }
    
    public String toString() {
        return "blue: ${blueTiles}, red: ${redTiles}, empty: ${emptyTiles}, total: ${totalTiles}"
    }
    
}
