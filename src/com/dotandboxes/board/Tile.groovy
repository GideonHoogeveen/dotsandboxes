package com.dotandboxes.board

import groovy.transform.CompileStatic
import groovy.transform.ToString


@CompileStatic
@ToString
class Tile {
    
    Map<Direction,Line> directions = [:]
    Line fillColor = Line.NONE
    
    public Tile() {
        Direction.values().each { Direction dir ->
            directions << [(dir): Line.NONE]
        }
    }
    
    private Tile(Tile otherTile) {
        this.fillColor = otherTile.fillColor
        Direction.values().each { Direction dir ->
            directions << [(dir):otherTile.directions[dir]]
        }
    }
    
    public Line getLine(Direction dir) {
        return directions[dir]
    }
    
    public boolean isFull() {
        return !directions.values().contains(Line.NONE)
    }
    
    public Collection<Direction> getEmptyDirections() {
        return Direction.values().findAll { Direction dir ->
             directions[dir].isEmpty()
        }
    }
    
    public void drawLine(Direction direction, Line line) {
        directions[direction] = line
        
        if(isFull()) {
            fillColor = line
        }
    }
    
    public Tile clone() {
        return new Tile(this)
    }
    
    
}
