package com.dotandboxes.board

import groovy.transform.CompileStatic

import java.awt.Color

@CompileStatic
enum Line {
    RED(Color.RED, new Color(255,127,127)), BLUE(Color.BLUE, new Color(127,127,255)), NONE(Color.LIGHT_GRAY, Color.WHITE)
    
    //TODO: remove dependency on Color
    public final Color lineColor
    public final Color fillColor
    
    private Line(Color lineColor, Color fillColor) {
        this.lineColor = lineColor
        this.fillColor = fillColor
    }
    
    public isEmpty() {
        return this == NONE
    }
    
    public isDrawn() {
        return this != NONE
    }
    
    public Line switchColor() {
        if(this == RED) {
            return BLUE
        }
        if(this == BLUE) {
            return RED
        }
        return NONE
    }
    
}
