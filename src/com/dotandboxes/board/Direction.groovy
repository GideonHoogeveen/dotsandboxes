package com.dotandboxes.board

import groovy.transform.CompileStatic

@CompileStatic
enum Direction {
    NORTH(0,-1), EAST(1,0), SOUTH(0,1), WEST(-1,0)
    
    public static final int SIZE = values().size()
    
    public final int dx
    public final int dy
    
    public Direction(int dx, int dy) {
        this.dx = dx
        this.dy = dy
    }
    
    Direction clockWise() {
        return values()[rotateIndex(1)]
    }
    
    Direction mirrored() {
        return values()[rotateIndex(2)]
    }
    
    Direction counterClockWise() {
        return values()[rotateIndex(3)]
    }
    
    private int rotateIndex(int amount) {
        return (ordinal() + amount) % SIZE
    }
    
}
