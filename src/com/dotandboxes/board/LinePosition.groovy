package com.dotandboxes.board

import groovy.transform.CompileStatic
import groovy.transform.Immutable;

/**
 * Provides a way to identify a specific Line on a Board
 */
@CompileStatic
@Immutable
class LinePosition {
    
    TilePosition position
    Direction direction
    
    public LinePosition mirror() {
        return new LinePosition(new TilePosition(position.x + direction.dx, position.y + direction.dy), direction.mirrored())
    }
}
