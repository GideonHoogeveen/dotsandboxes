package com.dotandboxes.board

import groovy.transform.CompileStatic
import groovy.transform.Immutable

@CompileStatic
@Immutable
class TilePosition {

    int x
    int y
    
    public TilePosition plus(Direction direction) {
        return new TilePosition(x + direction.dx, y + direction.dy)
    }
    
    
}
