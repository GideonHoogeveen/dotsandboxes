package com.dotandboxes.board

import groovy.transform.CompileStatic

import com.utils.LoopUtils

@CompileStatic
class DotAndBoxesBoard {
    
    public final int width
    public final int height
    private Tile[][] board
    public List<LinePosition> history = []
    
    public DotAndBoxesBoard(int width, int height) {
        assert width > 0
        assert height > 0
        
        this.width = width
        this.height = height        
        board = new Tile[width][height]
        
        LoopUtils.doubleRangeLoop(width, height) { int x, int y ->
            board[x][y] = new Tile()
        }
    }
    
    private DotAndBoxesBoard(DotAndBoxesBoard otherBoard) {
        this.width = otherBoard.width
        this.height = otherBoard.height
        this.history.addAll(otherBoard.history)
        board = new Tile[width][height]
        
        LoopUtils.doubleRangeLoop(width, height) { int x, int y ->
            board[x][y] = otherBoard.board[x][y].clone()
        }
    }
    
    public boolean isValidPosition(LinePosition linePos) {
        return isValidPosition(linePos.position)
    }
    
    public boolean isValidPosition(TilePosition position) {
        return isValidPosition(position.x, position.y)
    }
    
    public boolean isValidPosition(int x, int y) {
        return (x >=0 && x < width && y >= 0 && y < height)
    }
    
    public boolean isDrawn(LinePosition linePos) {
        return getLine(linePos).isDrawn()
    }
    
    public boolean isEmpty(LinePosition linePos) {
        return getLine(linePos).isEmpty()
    }
    
    public Line getLine(LinePosition linePos) {
        return getLine(linePos.position, linePos.direction)
    }
    
    public Line getLine(TilePosition position, Direction direction) {
        return board[position.x][position.y].getLine(direction)
    }
    
    public Line getFill(TilePosition position) {
        return board[position.x][position.y].fillColor
    }
    
    public List<TilePosition> getAdjacentTiles(LinePosition linePos) {
        LinePosition mirrored = linePos.mirror()
        if(isValidPosition(mirrored)) {
            return [linePos.position, mirrored.position]
        }
        return [linePos.position]
    }
    
    public Collection<LinePosition> getAllLinePositions(TilePosition position) {
        return Direction.values().collect { Direction direction ->
            return new LinePosition(position, direction)
        }
    }
    
    public Collection<LinePosition> getEmptyLinePositions(TilePosition position) {
        return board[position.x][position.y].getEmptyDirections().collect {Direction direction ->
            return new LinePosition(position, direction)
        }
    }
    
    public boolean isCompletelyFull() {
        boolean isFull = true
        LoopUtils.doubleRangeLoop(width, height) { int x, int y ->
            if(!board[x][y].isFull()) {
                isFull = false
            }
        }
        return isFull
    }
        
    public boolean isTileFilled(TilePosition position) {
        return isTileFilled(position.x, position.y)
    }
    
    public boolean isTileFilled(int x, int y) {
        return board[x][y].isFull()
    }
    
    public drawLine(LinePosition linePos, Line line) {
        
        Direction direction = linePos.direction
        TilePosition pos = linePos.position
        
        board[pos.x][pos.y].drawLine(direction, line)
        
        int newX = pos.x + direction.dx
        int newY = pos.y + direction.dy
        
        if(isValidPosition(newX, newY)) {
            board[newX][newY].drawLine(direction.mirrored(), line)
        }
        
        history << linePos
    }
    
    public boolean isWon() {
        getStatistics().isWon()
    }
    
    public BoardStatistics getStatistics() {
        Map<Line, Integer> result = [(Line.BLUE):0, (Line.RED):0, (Line.NONE): 0]
        LoopUtils.doubleRangeLoop(width, height) { int x, int y ->
            result[board[x][y].getFillColor()] += 1
        }
        return new BoardStatistics(result[Line.NONE], result[Line.BLUE], result[Line.RED])
    }
    
    public DotAndBoxesBoard clone() {
        return new DotAndBoxesBoard(this)
    }
    
}
