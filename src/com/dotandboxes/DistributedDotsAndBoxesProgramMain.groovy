package com.dotandboxes


import groovy.transform.CompileStatic

import com.dotandboxes.agents.DotAndBoxesAgent
import com.dotandboxes.agents.ExampleAgent
import com.dotandboxes.agents.OneBigMistakeAgent
import com.dotandboxes.agents.PrettySolidAgent
import com.dotandboxes.agents.SafetyAgentWrapper
import com.dotandboxes.display.DotAndBoxesFrame
import com.dotandboxes.game.Tournament

@CompileStatic
class DistributedDotsAndBoxesProgramMain {

    static main(args) {
        //Instantiate some existing agents. (There a lot off strong agents to compete against.)
        //List<DotAndBoxesAgent> agents = [new BrutalAgent(), new GeertsAgentNewest(), new DoeMaarWatAgent(), new MrCroissantAgent(), new OneBigMistakeAgent(), new WicoAgent(), new StefanAgent(), new HansAgent()] as List<DotAndBoxesAgent>
        List<DotAndBoxesAgent> agents = [new PrettySolidAgent(),new OneBigMistakeAgent()].collect{it} as List<DotAndBoxesAgent>
        
        //Instantiate your agent. Wrap it into a SafetyWrapper that stops your agent from crashing the application.
        DotAndBoxesAgent ownAgent = new SafetyAgentWrapper( new ExampleAgent() )
        
        //Add your own agent to the list of agents
        //agents << ownAgent
        
        //Create a tournament with the given agents
        Tournament tournament = new Tournament(agents, 15, 2) //Boardsize will be 9 x 9 and amount of matches between two agents 2
        
        //Creates a frame that displays the game. comment out to disable display
        new DotAndBoxesFrame(tournament.gameManager)
        
        //Wait 2 seconds before starting the games
        Thread.sleep(2000)
        
        //Start the tournament and wait 750ms between each move and 2000 ms when a match is over.
        tournament.startGames(0,2000)
    }
}
