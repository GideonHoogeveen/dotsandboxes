package com.dotandboxes.graph

class Graph<N> {
    
    List<Node<N>> nodes = []
    
    public Node<N> getNode(N nodeValue) {
        return nodes.find { it.value == nodeValue }
    }
    
    public void addNode(Node<N> node) {
        nodes << node
    }
        
}
