package com.dotandboxes.graph

import groovy.transform.CompileStatic;

@CompileStatic
class Node<N> {
    
    N value
    Set<Node<N>> nodes = ([] as Set<Node<N>>)
    
    public Node(N value) {
        this.value = value
    }
    
    public add(Node<N> node) {
        nodes << node
    }
    
    public int getPathLength() {
        if(nodes.size() == 0) {
            return 1
        }
        
        if (nodes.size() > 2){
            return 0
        }
        
        int count = 1
        List<Node<N>> frontier = []
        frontier.addAll(nodes)
        
        Set<N> alreadyChecked = []
        alreadyChecked.add(this.value)
        
        while(frontier) {
            Node node = frontier.pop()
            
            if(!(node.value in alreadyChecked)) {
                if(node.nodes.size() <= 2) {
                    frontier.addAll(node.nodes)
                    count ++
                }
                alreadyChecked << node.value
            }
        }
        
        return count
    }
    
}
