package com.dotandboxes.display

import groovy.transform.CompileStatic

import java.awt.BasicStroke
import java.awt.Color
import java.awt.Dimension
import java.awt.Graphics
import java.awt.Graphics2D

import javax.swing.JPanel

import com.dotandboxes.board.BoardStatistics
import com.dotandboxes.board.Direction
import com.dotandboxes.board.DotAndBoxesBoard
import com.dotandboxes.board.Line
import com.dotandboxes.board.TilePosition
import com.dotandboxes.game.DotAndBoxesGame
import com.dotandboxes.game.GameManager

@CompileStatic
class DotAndBoxesPanel extends JPanel implements Observer {
    
    public static final int TILE_SIZE = 60
    public static final int TILE_OFFSET = 12
    
    
    private GameManager manager
    
    public DotAndBoxesPanel(GameManager manager) {
        this.manager = manager
        manager.addObserver(this)
        
        DotAndBoxesGame game = manager.currentGame
        int width = (TILE_SIZE + 1) * game.board.width + TILE_OFFSET * 2 + 10
        int height = (TILE_SIZE + 1) * game.board.height + TILE_OFFSET * 2 + 38
        this.setSize(width + TILE_SIZE *5, height)
        this.setPreferredSize(new Dimension(width + TILE_SIZE *5, height))
    }
    
    @Override
    public void paintComponent(Graphics g){
        super.paintComponent(g);
        
        Graphics2D g2 = (Graphics2D) g;
        
        g2.setStroke(new BasicStroke(12))
        
        DotAndBoxesGame currentGame = manager.currentGame
        DotAndBoxesBoard board = manager.currentGame?.getBoard()
        
        
        if(!board) {
            return
        }
        
        g.setFont(g.getFont().deriveFont(40.0f))
        List<String> names = manager.currentGame.getAgentNames()
        g.setColor(Line.BLUE.lineColor)
        g.drawString(names[0], board.width * (TILE_SIZE) + TILE_OFFSET + TILE_SIZE, 75)
        g.setColor(Line.RED.lineColor)
        g.drawString(names[1], board.width * (TILE_SIZE) + TILE_OFFSET + TILE_SIZE, 150)
        BoardStatistics stats = board.getStatistics()
        
        
        g.setColor(Line.BLUE.lineColor)
        g.drawString("blue: ${stats.blueTiles}", board.width * (TILE_SIZE) + TILE_OFFSET + TILE_SIZE, 225)
        
        g.setColor(Line.RED.lineColor)
        g.drawString("red: ${stats.redTiles}", board.width * (TILE_SIZE) + TILE_OFFSET + TILE_SIZE *3, 225)
        
        if(currentGame.isOver()) {
            g.setColor(new Color(0, 127, 0))
            g.drawString("${currentGame.getWinnerName()} won!", board.width * (TILE_SIZE) + TILE_OFFSET + TILE_SIZE, 300)
        }
        
        (0..<board.width).each { int x ->
            (0..<board.height).each { int y ->
                int leftX = x*(TILE_SIZE) + TILE_OFFSET
                int upY = y*(TILE_SIZE) + TILE_OFFSET
                int rightX = leftX + TILE_SIZE
                int downY = upY + TILE_SIZE
                
                //TODO: Flyweight to reduce BoardPosition object creation? Measure difference.
                TilePosition position = new TilePosition(x, y)
                
                g.setColor(board.getFill(position).fillColor)
                g.fillRect(leftX, upY, TILE_SIZE, TILE_SIZE)
                
                g.setColor(board.getLine(position,Direction.NORTH).lineColor)
                g.drawLine(leftX, upY, rightX, upY)
                
                g.setColor(board.getLine(position,Direction.SOUTH).lineColor)
                g.drawLine(leftX, downY, rightX, downY)
                
                g.setColor(board.getLine(position,Direction.WEST).lineColor)
                g.drawLine(leftX, upY, leftX, downY)
                
                g.setColor(board.getLine(position,Direction.EAST).lineColor)
                g.drawLine(rightX, upY, rightX, downY)
            }
        }
        
    }

    @Override
    public void update(Observable o, Object arg) {
        this.invalidate()
        this.repaint()
    }
}
