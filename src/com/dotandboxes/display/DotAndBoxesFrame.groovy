package com.dotandboxes.display

import groovy.transform.CompileStatic

import java.awt.Dimension

import javax.swing.BoxLayout
import javax.swing.JFrame
import javax.swing.JPanel

import com.dotandboxes.game.DotAndBoxesGame
import com.dotandboxes.game.GameManager

@CompileStatic
class DotAndBoxesFrame extends JFrame {
    
    JPanel boardPanel
    
    public DotAndBoxesFrame(GameManager manager) {
        super("Dots and Boxes");
        
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        boardPanel = new DotAndBoxesPanel(manager)
        
        setContentPane(boardPanel)
        pack()
        
        setVisible(true);
    }
    
}

