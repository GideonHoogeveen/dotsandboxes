package com.dotandboxes.game

import com.dotandboxes.agents.DotAndBoxesAgent
import com.dotandboxes.board.DotAndBoxesBoard
import com.dotandboxes.display.DotAndBoxesFrame

class Tournament {
    
    GameManager gameManager
    DotAndBoxesBoard board
    
    public Tournament(List<DotAndBoxesAgent> agents, int boardSize, int amountOfGames) {
        assert agents.size() >= 2
        gameManager = new GameManager()
        board = new DotAndBoxesBoard(boardSize,boardSize)
        
        addGamesBetween(agents, amountOfGames)
    }
    
    private void addGamesBetween(List<DotAndBoxesAgent> agents, int amountOfGames) {
        while(agents.size()) {
            DotAndBoxesAgent agent = agents.pop()
            agents.each { DotAndBoxesAgent other ->
                addGamesBetween(agent, other, amountOfGames)
            }
        }
    }
    
    private void addGamesBetween(DotAndBoxesAgent agent, List<DotAndBoxesAgent> agents, int amountOfGames) {
        agents.each { DotAndBoxesAgent other ->
            addGamesBetween(agent, other, amountOfGames)
        }
    }
    
    private void addGamesBetween(DotAndBoxesAgent agent1, DotAndBoxesAgent agent2, int amountOfGames) {
        List<DotAndBoxesAgent> agents = [agent1, agent2]
        (0..<amountOfGames).each {
            gameManager.addGame(new DotAndBoxesGame(board.clone(), agents[0], agents[1]))
            agents << agents.remove(0) //balance by switching the color agents have to play
        }
    }
    
    public startGames(int frameSleepTime = 200, int gameOverSleepTime = 1000) {
        
        while(gameManager.gamesLeft()) {
            gameManager.update()
            Thread.sleep(frameSleepTime)
            if(gameManager.isCurrentGameOver()) {
                Thread.sleep(gameOverSleepTime)
            }
        }
        
        //printing result
        Map<DotAndBoxesAgent, Integer> winCount = [:]
        Map<DotAndBoxesAgent, Integer> loseCount = [:]
        
        gameManager.playedGames.each { DotAndBoxesGame game ->
            GameStatistics stats = game.getStatistics()
            if(!winCount[stats.winner]) {
                winCount[stats.winner] = 0
            }
            if(!loseCount[stats.loser]) {
                loseCount[stats.loser] = 0
            }
            winCount[stats.winner] += 1
            loseCount[stats.loser] += 1
        }
        
        println "win count: ${winCount}"
        println "lose count: ${loseCount}"
    }
    
}
