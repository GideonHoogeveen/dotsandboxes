package com.dotandboxes.game

import groovy.transform.CompileStatic

import com.dotandboxes.agents.DotAndBoxesAgent
import com.dotandboxes.board.BoardStatistics;
import com.dotandboxes.board.DotAndBoxesBoard
import com.dotandboxes.board.Line
import com.dotandboxes.board.LinePosition
import com.utils.LoopUtils

@CompileStatic
class DotAndBoxesGame{
    
    private DotAndBoxesBoard board
    private Line playerTurn
    private Map<Line,DotAndBoxesAgent> agents
    private agentDisqualified = false
    
    public DotAndBoxesGame(DotAndBoxesBoard board, DotAndBoxesAgent blueAgent, DotAndBoxesAgent redAgent) {
        this.board = board.clone()
        this.playerTurn = Line.BLUE
        agents = [(Line.BLUE):blueAgent, (Line.RED):redAgent]
    }
    
    public List<String> getAgentNames() {
        return [agents[Line.BLUE].getName(), agents[Line.RED].getName()]
    }
    
    public DotAndBoxesBoard getBoard() {
        return board.clone()
    }
    
    public void update() {
        if(isOver()) {
            throw new GameAlreadyOverException("We can't make any updates to the game state because the game is already over")
        }
        makeMove()
    }
    
    public boolean isOver() {
        return board.isCompletelyFull() || board.isWon() || agentDisqualified
    }
    
    public Line getWinner() {
        return agentDisqualified? playerTurn.switchColor() : board.getStatistics().getWinningColor()
    }
    
    public String getWinnerName() {
        return agents[getWinner()].getName()
    }
        
    public GameStatistics getStatistics() {
        Line winningLine = getWinner()
        return new GameStatistics(agents[winningLine], agents[winningLine.switchColor()], winningLine, winningLine.switchColor())
    }
    
    public DotAndBoxesAgent getAgentToMove() {
        return agents[playerTurn]
    }
    
    private void makeMove() {
        DotAndBoxesAgent agent = getAgentToMove()
        
        LinePosition linePos = agent.getAction(board.clone())
        
        /*
        if(!linePos) {
            throw new InvalidActionException(agent, "agent: ${agent} returned an action of null")
        }
        
        if(!board.isValidPosition(linePos.position)) {
            throw new InvalidActionException(agent, "agent: ${agent} returned an action with a position x:${linePos.position.x}, y:${linePos.position.y} that is not on the board")
        }
        
        if(board.getLine(linePos).isDrawn()) {
            throw new InvalidActionException(agent, "agent: ${agent} returned an action that draws on an already drawn line. x:${linePos.position.x}, y:${linePos.position.y}, dir: ${linePos.direction}")
        }*/
        
        if(!isValidMove(linePos)) {
            println "agent: ${agent} lost the game because he made an invalid action. (returned no action or returned an action not on the board or returned an action that draws on a line that has already been drawn on)"
            agentDisqualified = true
            return
        }
        
        board.drawLine(linePos, playerTurn)
        
        if(!board.isTileFilled(linePos.position)) {
            if(board.isValidPosition(linePos.mirror().position)) {
                if(!board.isTileFilled(linePos.mirror().position)) {
                    playerTurn = playerTurn.switchColor()
                }
            } else {
                playerTurn = playerTurn.switchColor()
            }
        }
    }
    
    private boolean isValidMove(LinePosition linePos) {
        return (linePos && board.isValidPosition(linePos.position) && board.getLine(linePos).isEmpty())
    }
    
}
