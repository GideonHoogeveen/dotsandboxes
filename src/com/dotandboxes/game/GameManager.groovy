package com.dotandboxes.game

import groovy.transform.CompileStatic;

import java.util.Observable;

import com.dotandboxes.display.DotAndBoxesFrame

@CompileStatic
class GameManager extends Observable {

    List<DotAndBoxesGame> games = []
    DotAndBoxesGame currentGame
    List<DotAndBoxesGame> playedGames = []
    
    public void addGame(DotAndBoxesGame game) {
        if(!currentGame) {
            currentGame = game
        } else {
            games << game
        }
    }
    
    public boolean gamesLeft() {
        return !currentGame.isOver() || !games.isEmpty()
    }
    
    public boolean isCurrentGameOver() {
        return currentGame.isOver()
    }
    
    public void update() {
        if(!currentGame) {
            return
        }
        
        if(currentGame.isOver() && games) {
            currentGame = games.remove(0)
        }
        
        currentGame.update()
        setChanged()
        notifyObservers()
        
        if(currentGame.isOver()) {
            playedGames << currentGame
        }
    }
    
}
