package com.dotandboxes.game

import groovy.transform.CompileStatic;

@CompileStatic
class GameAlreadyOverException extends RuntimeException {

    public GameAlreadyOverException() {
        super()
    }
    
    public GameAlreadyOverException(String message) {
        super(message)
    }
    
}
