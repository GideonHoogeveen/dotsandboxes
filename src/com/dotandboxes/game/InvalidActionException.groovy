package com.dotandboxes.game

import groovy.transform.CompileStatic;

import com.dotandboxes.agents.DotAndBoxesAgent;

@CompileStatic
class InvalidActionException extends RuntimeException {

    DotAndBoxesAgent agent
    
    public InvalidActionException(DotAndBoxesAgent agent) {
        super()
        this.agent = agent
    }
    
    public InvalidActionException(DotAndBoxesAgent agent, String message) {
        super(message)
        this.agent = agent
    }
    
}
