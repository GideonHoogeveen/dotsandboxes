package com.dotandboxes.game

import groovy.transform.CompileStatic

import com.dotandboxes.agents.DotAndBoxesAgent
import com.dotandboxes.board.Line

@CompileStatic
class GameStatistics {

    DotAndBoxesAgent winner
    DotAndBoxesAgent loser
    Line winnerLine
    Line loserLine
    
    public GameStatistics(DotAndBoxesAgent winner, DotAndBoxesAgent loser, Line winnerLine, Line loserLine) {
        this.winner = winner
        this.loser = loser
        this.winnerLine = winnerLine
        this.loserLine = loserLine
    }
    
    public String toString() {
        "${winner.getName()}(${winnerLine}) won and ${loser.getName()}(${loserLine}) lost"
    }
    
}
