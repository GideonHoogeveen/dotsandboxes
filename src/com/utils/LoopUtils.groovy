package com.utils

import groovy.transform.CompileStatic;

@CompileStatic
class LoopUtils {
    
    public static doubleRangeLoop(int maxX, int maxY, Closure clos) {
        for(int x in (0..<maxX)) {
            for(int y in (0..<maxY)) {
                clos(x,y)
            }
        }
    }
    
}
